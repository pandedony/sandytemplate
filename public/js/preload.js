// var loader = document.getElementById("preloader");
// window.addEventListener("load", function(){
//     loader.style.display = "none"
// });

// $(document).ready(function(){
// $(".preloader").fadeOut();
// })

// $.get("/assets/html/loader.html", function(data){
//     $("#loading").replaceWith(data);
// });

// $(window).on('loader', function(){
//     setTimeout(removeLoader, 20); //wait for page load PLUS two seconds.
//   });
//   function removeLoader(){
//       $( "#preloader" ).fadeOut(100, function() {
//         // fadeOut complete. Remove the loading div
//         $( "#preloader" ).remove(); //makes page more lightweight 
//     });   
//   }
  var html5 = {
    getSources: function getSources() {
      var _this = this;

      if (!this.isHTML5) {
        return [];
      }

      var sources = Array.from(this.media.querySelectorAll('source')); // Filter out unsupported sources (if type is specified)

      return sources.filter(function (source) {
        var type = source.getAttribute('type');

        if (is$1.empty(type)) {
          return true;
        }

        return support.mime.call(_this, type);
      });
    },
    // Get quality levels
    getQualityOptions: function getQualityOptions() {
      // Whether we're forcing all options (e.g. for streaming)
      if (this.config.quality.forced) {
        return this.config.quality.options;
      } // Get sizes from <source> elements


      return html5.getSources.call(this).map(function (source) {
        return Number(source.getAttribute('size'));
      }).filter(Boolean);
    },
    setup: function setup() {
      if (!this.isHTML5) {
        return;
      }

      var player = this; // Set speed options from config

      player.options.speed = player.config.speed.options; // Set aspect ratio if fixed

      if (!is$1.empty(this.config.ratio)) {
        setAspectRatio.call(player);
      } // Quality


      Object.defineProperty(player.media, 'quality', {
        get: function get() {
          // Get sources
          var sources = html5.getSources.call(player);
          var source = sources.find(function (s) {
            return s.getAttribute('src') === player.source;
          }); // Return size, if match is found

          return source && Number(source.getAttribute('size'));
        },
        set: function set(input) {
          if (player.quality === input) {
            return;
          } // If we're using an an external handler...


          if (player.config.quality.forced && is$1.function(player.config.quality.onChange)) {
            player.config.quality.onChange(input);
          } else {
            // Get sources
            var sources = html5.getSources.call(player); // Get first match for requested size

            var source = sources.find(function (s) {
              return Number(s.getAttribute('size')) === input;
            }); // No matching source found

            if (!source) {
              return;
            } // Get current state


            var _player$media = player.media,
                currentTime = _player$media.currentTime,
                paused = _player$media.paused,
                preload = _player$media.preload,
                readyState = _player$media.readyState,
                playbackRate = _player$media.playbackRate; // Set new source

            player.media.src = source.getAttribute('src'); // Prevent loading if preload="none" and the current source isn't loaded (#1044)

            if (preload !== 'none' || readyState) {
              // Restore time
              player.once('loadedmetadata', function () {
                player.speed = playbackRate;
                player.currentTime = currentTime; // Resume playing

                if (!paused) {
                  silencePromise(player.play());
                }
              }); // Load new source

              player.media.load();
            }
          } // Trigger change event


          triggerEvent.call(player, player.media, 'qualitychange', false, {
            quality: input
          });
        }
      });
    },
    // Cancel current network requests
    // See https://github.com/sampotts/plyr/issues/174
    cancelRequests: function cancelRequests() {
      if (!this.isHTML5) {
        return;
      } // Remove child sources


      removeElement(html5.getSources.call(this)); // Set blank video src attribute
      // This is to prevent a MEDIA_ERR_SRC_NOT_SUPPORTED error
      // Info: http://stackoverflow.com/questions/32231579/how-to-properly-dispose-of-an-html5-video-and-close-socket-or-connection

      this.media.setAttribute('src', this.config.blankVideo); // Load the new empty source
      // This will cancel existing requests
      // See https://github.com/sampotts/plyr/issues/174

      this.media.load(); // Debugging

      this.debug.log('Cancelled network requests');
    }
  };







$(window).on('load', function () {
    $(".loader").fadeOut();
    $("#preloder").delay(200).fadeOut("slow");
});