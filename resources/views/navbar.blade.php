
{{-- <header class="p-3 bg-dark text-white">
    <div class="container">
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarScroll" aria-controls="navbarScroll" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
      <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
        <a href="/" class="d-flex align-items-center mb-2 mb-lg-0 text-white text-decoration-none">
          <svg class="bi me-2" width="40" height="32" role="img" aria-label="Bootstrap"><use xlink:href="#bootstrap"/></svg>
        </a>

        <ul class="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0">
          <li nav-item><a class="nav-link px-2 text-secondary {{ Request::is('/') ? 'active' : '' }}" href="">Home</a></li>
          <li nav-item><a class="nav-link px-2 text-secondary {{ Request::is('/about') ? 'active' : '' }}" href="#about">About</a></li>
          <li nav-item><a class="nav-link px-2 text-secondary {{ Request::is('/price') ? 'active' : '' }}" href="#">Pricing</a></li>
          <li nav-item><a class="nav-link px-2 text-secondary {{ Request::is('/contact') ? 'active' : '' }}" href="#">Contact Us</a></li>
        </ul>

        <form class="col-12 col-lg-auto mb-3 mb-lg-0 me-lg-3">
          <input type="search" class="form-control form-control-dark" placeholder="Search..." aria-label="Search">
        </form>

        <div class="text-end">
          <button type="button" class="btn btn-outline-light me-2">Login</button>
          <button type="button" class="btn btn-warning">Sign-up</button>
        </div>
      </div>
    </div>
  </header> --}}
  
  
  <nav id="mainNav" class="navbar navbar-expand-lg" style="position: fixed; left: 0; top: 0; right: 0; z-index: 1000;">
      <div class="container">
        {{-- <a class="navbar-brand" href="/">Meyaboo</a> --}}
        <img src="assets/logo-meyaboo-hitam.png" alt="" width="50">
        <button class="navbar-toggler navbar-toggler-bg" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav mx-2">
            <li class="nav-item">
              <a class="nav-link btn" href="#">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link btn" href="#about">About</a>
            </li>
            <li class="nav-item">
              <a class="nav-link btn" href="#product">Product</a>
            </li>
            <li class="nav-item">
              <a class="nav-link btn" href="#price">Pricing</a>
            </li>
            <li class="nav-item">
              <a class="nav-link btn" href="#contact">Contact Us</a>
            </li>
          </ul>
          <ul class="navbar-nav ms-auto">
              <div class="text-end">
                  <button type="button" class="btn-log btn-login me-2">Login</button>
                  <button type="button" class="btn-log btn-signup">Sign-up</button>
              </div>
          </ul>
        </div>
      </div>
    </nav>

{{-- <nav class="navbar navbar-expand-lg bg-dark navbar-dark">
    <div class="container">
      <a class="navbar-brand" href="#">Navbar scroll</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarScroll" aria-controls="navbarScroll" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarScroll">
        <ul class="navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll" style="--bs-scroll-height: 100px;">
          <li class="nav-item">
            <a class="nav-link {{ Request::is('/') ? 'active' : '' }}" aria-current="page" href="#">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link {{ Request::is('/about') ? 'active' : '' }}" href="#">About</a>
          </li>
          <li class="nav-item">
            <a class="nav-link {{ Request::is('/price') ? 'active' : '' }}" href="#">Pricing</a>
          </li>
          <li class="nav-item">
            <a class="nav-link {{ Request::is('/contact') ? 'active' : '' }}" href="#">Contact Us</a>
          </li>
        </ul>
          <div class="navbar-nav dropdown">
              <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" data-bs-toggle="dropdown" aria-expanded="false"><i class="bi bi-person-fill"></i></a>
                  <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdownMenulink">
                      <li><a class="dropdown-item" href="#">Register</a></li>
                      <li><a class="dropdown-item" href="#">Login</a></li>
                    </ul>
                </li>
            </div>
      </div>
    </div>
  </nav> --}}
